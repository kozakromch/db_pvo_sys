
\! sudo useradd -M chief
\! sudo useradd -M pvoreader

CREATE GROUP pvoreaders;
CREATE GROUP chiefs;

CREATE USER pvoreader WITH PASSWORD '1234' NOCREATEDB NOCREATEUSER;
CREATE USER chief WITH PASSWORD '1234' NOCREATEDB NOCREATEUSER;

ALTER GROUP chiefs ADD USER chief;
ALTER GROUP pvoreaders ADD USER pvoreader;

GRANT SELECT ON public.inactive_targets,
				public.active_tasks,
				public.full_target,
				public.to_high_target TO GROUP pvoreaders;


GRANT ALL ON public.inactive_targets,
				public.active_tasks,
				public.full_target,
				public.to_high_target,
				piblic.not_parsed_target TO GROUP chiefs;


GRANT ALL ON TABLE	public.target TO GROUP chiefs;


\echo ------------------------------------------------------------------------------
\echo pvoreader
\echo ------------------------------------------------------------------------------


\c - pvoreader


\echo inactive_targets
SELECT * FROM inactive_targets;

\echo active_targets
SELECT * FROM active_targets;

\echo full_target
SELECT * FROM full_target;

\echo not_parsed_target
SELECT * FROM not_parsed_target;

\echo to_high_target
SELECT * FROM to_high_target;


\echo ------------------------------------------------------------------------------
\echo FORBIDED
\echo ------------------------------------------------------------------------------

\echo
\echo TARGET
\echo
SELECT * FROM TARGET;

\echo
\echo IAP_SYS
\echo
SELECT * FROM iap_sys;


\echo ------------------------------------------------------------------------------
\echo CHIEF
\echo ------------------------------------------------------------------------------


\c - chief

\echo
\echo TARGET
\echo
SELECT * FROM TARGET;

\echo
\echo IAP_SYS
\echo
SELECT * FROM iap_sys;
