import random
file = open("insert_target.sql", 'w')
max_numb = 50
	 
for i in range(1, max_numb):
	file.write("\n INSERT INTO target \n VALUES ")
	file.write("\n	" + "(nextval(\'id_seq\')," 
 		 + str(random.randint(0,100)) + ","
 		 + str(random.randint(0,100)) + ","
 		 + str(random.randint(0,25)) + ","
 		 + str(random.randint(0,50)) + ","
 		 + str(random.randint(70,200)) + ","
 		 + "TRUE" + ");")

file.close()

file = open("update_target.sql", 'w')
max_numb = 50
	 
for i in range(1, int(max_numb/2)):
	file.write("\n UPDATE target SET status = FALSE WHERE id = " + str(i*2) + ";" + "\n")
file.close()
