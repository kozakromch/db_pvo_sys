DROP FUNCTION  IF EXISTS getDistance (float,float,float,float);

CREATE FUNCTION getDistance(x1 float, y1 float, x2 float, y2 float)
RETURNS float AS '
    DECLARE
        dist float = 0;
    BEGIN
        IF x1 = x2 AND y1 = y2
            THEN RETURN dist;
        ELSE
            dist = sqrt(((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1))); 

            RETURN dist;
        END IF;
    END;
' 

LANGUAGE plpgsql;


DROP TRIGGER IF EXISTS parsed_target_trig ON target;
CREATE OR REPLACE FUNCTION parse_target_func() RETURNS trigger
    AS'

    DECLARE
        distance float = 0;
        accept BOOL := false;
        subsys_iap iap_sys%rowtype;
        subsys_rtv rtv_sys%rowtype;
        subsys_zro zro_sys%rowtype;
        subsys_reb reb_sys%rowtype;
        
        sys_id integer = NULL;
        real_parsed_id integer = 0;
    BEGIN
        real_parsed_id = nextval(''parsed_id_seq'');
            INSERT INTO parsed_target (parsed_id)
            VALUES(real_parsed_id);
            
        for subsys_iap in (SELECT * FROM iap_sys) LOOP
            distance = getDistance(subsys_iap.coord_x, subsys_iap.coord_y, NEW.coord_x, NEW.coord_y);
            -- RAISE NOTICE ''distance = %'', distance;
            if (distance < sqrt(subsys_iap.square_observ/pi())) AND subsys_iap.state = 0 then 
                accept = true;
                sys_id = subsys_iap.iap_id;
            end if;
        END LOOP;
        if accept = false then
            raise notice ''No IAP subsys for target_id (%)'',NEW.id;
        end if;
        accept = false;
        UPDATE parsed_target SET parsed_iap_id = sys_id
        WHERE parsed_target.parsed_id = real_parsed_id;

        UPDATE IAP_sys SET state = 1
        WHERE iap_id = sys_id;
        
        sys_id = NULL;
        for subsys_rtv in (SELECT * FROM rtv_sys) LOOP
            distance = getDistance(subsys_rtv.coord_x, subsys_rtv.coord_y, NEW.coord_x, NEW.coord_y);
            -- RAISE NOTICE ''distance = %'', distance;
            if (distance < sqrt(subsys_rtv.square_observ/pi())) AND subsys_rtv.state = 0 then 
                accept = true;
                sys_id = subsys_rtv.rtv_id;
            end if;
        END LOOP;
        if accept = false then
            raise notice ''No RTV subsys for target_id (%)'',NEW.id;
        end if;
        accept = false;
        
        UPDATE parsed_target SET parsed_rtv_id = sys_id
        WHERE parsed_target.parsed_id = real_parsed_id;

        UPDATE rtv_sys SET state = 1
        WHERE rtv_id = sys_id;
       
        sys_id = NULL;
        for subsys_zro in (SELECT * FROM zro_sys) LOOP
            distance = getDistance(subsys_zro.coord_x, subsys_zro.coord_y, NEW.coord_x, NEW.coord_y);
            -- RAISE NOTICE ''distance = %'', distance;
            if (distance < sqrt(subsys_zro.square_observ/pi())) AND subsys_zro.state = 0 then 
                accept = true;
                sys_id = sys_id;
            end if;
        END LOOP;
        if accept = false then
            raise notice ''No ZRO subsys for target_id (%)'',NEW.id;
        end if;
        accept = false;
        
        
        UPDATE parsed_target SET parsed_zro_id = sys_id
        WHERE parsed_target.parsed_id = real_parsed_id;

        UPDATE zro_sys SET state = 1
        WHERE zro_id = sys_id;
       
        sys_id = NULL;
        for subsys_reb in (SELECT * FROM reb_sys) LOOP
            distance = getDistance(subsys_reb.coord_x, subsys_reb.coord_y, NEW.coord_x, NEW.coord_y);
            -- RAISE NOTICE ''distance = %'', distance;
            if (distance < sqrt(subsys_reb.square_observ/pi())) AND subsys_reb.state = 0 then 
                accept = true;
                sys_id = sys_id;
            end if;
        END LOOP;
        if accept = false then
            raise notice ''No REB subsys for target_id (%)'',NEW.id;
        end if;
        accept = false;
        
        UPDATE parsed_target SET parsed_reb_id = sys_id
        WHERE parsed_target.parsed_id = real_parsed_id;

        UPDATE reb_sys SET state = 1
        WHERE reb_id = sys_id;
        sys_id = NULL;

        UPDATE target SET parsed_target_id = real_parsed_id
        WHERE target.id = NEW.id; 
        RETURN NEW;
    END;
    '
    LANGUAGE plpgsql;

CREATE TRIGGER parsed_target_trig AFTER INSERT ON target
    FOR EACH ROW EXECUTE PROCEDURE parse_target_func ();


DROP TRIGGER IF EXISTS update_target_trig ON target;
CREATE OR REPLACE FUNCTION update_target_func() RETURNS trigger
    AS'
    BEGIN
        if NEW.status = FALSE then
            UPDATE iap_sys SET state = 0
            WHERE iap_sys.iap_id IN
                (SELECT parsed_iap_id FROM parsed_target
                WHERE NEW.parsed_target_id = parsed_target.parsed_id );

            UPDATE rtv_sys SET state = 0
            WHERE rtv_sys.rtv_id IN
                (SELECT parsed_rtv_id FROM parsed_target
                WHERE NEW.parsed_target_id = parsed_target.parsed_id );

            UPDATE zro_sys SET state = 0
            WHERE zro_sys.zro_id IN
                (SELECT parsed_zro_id FROM parsed_target
                WHERE NEW.parsed_target_id = parsed_target.parsed_id );

            UPDATE reb_sys SET state = 0
            WHERE reb_sys.reb_id IN
                (SELECT parsed_reb_id FROM parsed_target
                WHERE NEW.parsed_target_id = parsed_target.parsed_id );
        end if;
        RETURN NEW;
    END;
    '
    LANGUAGE plpgsql;
CREATE TRIGGER update_target_trig AFTER UPDATE ON target
    FOR EACH ROW EXECUTE PROCEDURE update_target_func ();
