DROP VIEW IF EXISTS inactive_targets;
DROP VIEW IF EXISTS active_targets;
DROP VIEW IF EXISTS full_target;
DROP VIEW IF EXISTS not_parsed_target;
DROP VIEW IF EXISTS to_high_target;

CREATE VIEW inactive_targets AS
	SELECT id AS target_id FROM target
	WHERE status = false;

CREATE VIEW active_targets AS
	SELECT id AS target_id FROM target 
	WHERE status = true;


CREATE VIEW full_target AS
	SELECT id, speed, acceleration, coord_x, coord_y, height, parsed_iap_id, parsed_rtv_id,parsed_zro_id,parsed_reb_id  FROM target
	LEFT JOIN  parsed_target ON
	parsed_id = parsed_target_id
	WHERE status = true;


CREATE VIEW not_parsed_target AS
	SELECT DISTINCT target.id FROM target, parsed_target
	WHERE parsed_iap_id = NULL AND
	parsed_rtv_id = NULL AND
	parsed_zro_id = NULL AND
	parsed_reb_id = NULL;
	
CREATE VIEW to_high_target AS
	SELECT DISTINCT target.id FROM target
	WHERE target.height > (SELECT MAX(iap_sys.height_attack) FROM iap_sys)
	AND target.height > (SELECT MAX(rtv_sys.height_observ) FROM rtv_sys)
	AND target.height > (SELECT MAX(zro_sys.height_attack) FROM zro_sys)
	AND target.height > (SELECT MAX(reb_sys.height_attack) FROM reb_sys)
	ORDER BY target.id;

\echo inactive_targets
SELECT * FROM inactive_targets;

\echo active_targets
SELECT * FROM active_targets;

\echo full_target
SELECT * FROM full_target;

\echo not_parsed_target
SELECT * FROM not_parsed_target;

\echo to_high_target
SELECT * FROM to_high_target;


    
