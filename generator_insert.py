import random
file = open("insert.sql", 'w')
max_numb = 50
	 
file.write("\n INSERT INTO IAP_sys \n	(iap_id, coord_x, coord_y, square_attack, square_observ, height_attack, max_amount_targer) \n VALUES ")
for i in range(1, max_numb):
	file.write("\n	" + "(" + 
			str(i) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,25)) + ","
		 + str(random.randint(100,1900)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,10)) +
		 "),")
file.write("\n	(" + str(max_numb) + ",0,0,0,0,0,0);")


file.write("\n INSERT INTO RTV_sys \n	(rtv_id, coord_x, coord_y, square_observ, height_observ, max_amount_targer) \n VALUES ")
for i in range(1, max_numb):
	file.write("\n	" + "(" + 
		str(i) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(100,1900)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,10)) + 
		 "),")
file.write("\n	(" + str(max_numb) + ",0,0,0,0,0);")


file.write("\n INSERT INTO ZRO_sys \n	(zro_id, coord_x, coord_y, square_observ, height_attack, max_amount_targer) \n VALUES ")
for i in range(1, max_numb):
	file.write("\n	" + "(" + 
		str(i) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(100,1900)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,10)) +
		 "),")
file.write("\n	(" + str(max_numb) + ",0,0,0,0,0);")


file.write("\n INSERT INTO REB_sys \n	(reb_id, coord_x, coord_y, square_observ, height_attack, max_amount_targer) \n VALUES ")
for i in range(1, max_numb):
	file.write("\n	" + "(" + 
		str(i) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(100,1900)) + ","
		 + str(random.randint(0,100)) + ","
		 + str(random.randint(0,10)) +
		 "),")
file.write("\n	(" + str(max_numb) + ",0,0,0,0,0);")


file.close()
