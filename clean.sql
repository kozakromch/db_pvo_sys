drop user PVOreader;
drop user chief;

REVOKE SELECT ON public.inactive_targets,
				public.active_tasks,
				public.full_target,
				public.to_high_target TO GROUP PVOreaders;


REVOKE ALL ON public.inactive_targets,
				public.active_tasks,
				public.full_target,
				public.to_high_target,
				piblic.not_parsed_target TO GROUP chiefs;


GRANT ALL ON TABLE	public.target TO GROUP chiefs;

DROP GROUP chiefs;
DROP GROUP PVOreaders;

\! sudo deluser chief
\! sudo deluser PVOreader

