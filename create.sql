DROP VIEW IF EXISTS inactive_targets;
DROP VIEW IF EXISTS active_targets;
DROP VIEW IF EXISTS free_sys;
DROP VIEW IF EXISTS not_parsed_target;
DROP VIEW IF EXISTS to_high_target;


DROP TABLE IF EXISTS Target CASCADE;
DROP TABLE IF EXISTS Parsed_target CASCADE;
DROP TABLE IF EXISTS iap_sys CASCADE;
DROP TABLE IF EXISTS rtv_sys CASCADE;
DROP TABLE IF EXISTS zro_sys CASCADE;
DROP TABLE IF EXISTS reb_sys CASCADE;

CREATE TABLE "iap_sys" (
	"iap_id" SERIAL PRIMARY KEY,
	"coord_x" float NOT NULL,
	"coord_y" float NOT NULL,
	"square_attack" float NOT NULL,
	"square_observ" float NOT NULL,
	"height_attack" float NOT NULL,
	"state" integer NOT NULL DEFAULT '0',
	"max_amount_targer" integer NOT NULL
) WITH (
  OIDS=FALSE
);

CREATE TABLE "rtv_sys" (
	"rtv_id" SERIAL PRIMARY KEY,
	"coord_x" float NOT NULL,
	"coord_y" float NOT NULL,
	"square_observ" float NOT NULL,
	"height_observ" float NOT NULL,
	"state" integer NOT NULL DEFAULT '0',
	"max_amount_targer" integer NOT NULL
) WITH (
  OIDS=FALSE
);


CREATE TABLE "zro_sys" (
	"zro_id" SERIAL PRIMARY KEY,
	"coord_x" float NOT NULL,
	"coord_y" float NOT NULL,
	"square_observ" float NOT NULL,
	"height_attack" float NOT NULL,
	"state" integer NOT NULL DEFAULT '0',
	"max_amount_targer" integer NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "reb_sys" (
	"reb_id" SERIAL PRIMARY KEY,
	"coord_x" float NOT NULL,
	"coord_y" float NOT NULL,
	"square_observ" float NOT NULL,
	"height_attack" float NOT NULL,
	"state" integer NOT NULL DEFAULT '0',
	"max_amount_targer" integer NOT NULL
) WITH (
  OIDS=FALSE
);


CREATE TABLE "parsed_target" (
	"parsed_id" SERIAL PRIMARY KEY,
	"parsed_iap_id" integer ,
	"parsed_rtv_id" integer ,
	"parsed_zro_id" integer ,
	"parsed_reb_id" integer ,
	FOREIGN KEY (parsed_iap_id) REFERENCES iap_sys(iap_id),
	FOREIGN KEY (parsed_rtv_id) REFERENCES rtv_sys(rtv_id),
	FOREIGN KEY (parsed_zro_id) REFERENCES zro_sys(zro_id),
	FOREIGN KEY (parsed_reb_id) REFERENCES reb_sys(reb_id)
) WITH (
  OIDS=FALSE
);

CREATE TABLE "target" (
	"id" SERIAL PRIMARY KEY,
	"speed" float NOT NULL DEFAULT '0',
	"acceleration" float NOT NULL DEFAULT '0',
	"coord_x" float NOT NULL CHECK (coord_y > 0),
	"coord_y" float NOT NULL  CHECK (coord_x > 0),
	"height" float NOT NULL,
	"status" bool NOT NULL,
	"parsed_target_id" integer UNIQUE REFERENCES parsed_target(parsed_id) ON DELETE CASCADE ON UPDATE CASCADE
) WITH (
  OIDS=FALSE
);

DROP SEQUENCE IF EXISTS id_seq CASCADE;
DROP SEQUENCE IF EXISTS parsed_id_seq CASCADE;

CREATE SEQUENCE id_seq START 1;
CREATE SEQUENCE parsed_id_seq START 1;
